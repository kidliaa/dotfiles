"meta settings
set nocompatible
syntax on
filetype plugin indent on
let mapleader = ";"
nnoremap <leader>ev :vsp ~/.vimrc<cr>
nnoremap <leader>sv :source ~/.vimrc<cr>

"status lines setting
source ~/dotfiles/vim/statusline.vimrc

"shortcuts
source ~/dotfiles/vim/shortcuts.vimrc

"plugin for plugin management :vim-pathogen
execute pathogen#infect('~/.vim/bundleForPathogen/{}')
"plugin NERDTree
nnoremap <C-n> :NERDTreeToggle<CR>

"Vundle settings
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
"plugins start must list here
Plugin 'itchyny/lightline.vim'
Plugin 'jelera/vim-javascript-syntax'
Plugin 'maksimr/vim-jsbeautify'
Plugin 'einars/js-beautify'
Plugin 'kien/ctrlp.vim'
Plugin 'vimwiki/vimwiki'
Plugin 'Raimondi/delimitMate'
Plugin 'pangloss/vim-javascript'
Plugin 'Valloric/YouCompleteMe'
Plugin 'mxw/vim-jsx'
let g:jsx_ext_required = 0
let g:ycm_add_preview_to_completeopt=0
let g:ycm_confirm_extra_conf=0
set completeopt-=preview
Plugin 'scrooloose/syntastic'
" This does what it says on the tin. It will check your file on open too, not just on save.
" You might not want this, so just leave it out if you don't.
let g:syntastic_check_on_open=1
let g:syntastic_always_populate_loc_list=1
"plugins list end
call vundle#end()

"appearence
syntax enable
set number
set background=dark
let g:solarized_termcolors=256
colorscheme solarized




"editing
set tabstop=2
set shiftwidth=2
set expandtab
nnoremap <F10> gg=G
"editing f2e beautify
map <c-f> :call JsBeautify()<cr>

"Plugin vimwiki setting
let g:vimwiki_list = [{ 'path' : '$HOME/Dropbox/Apps/vim/vimwiki','path_html':'$HOME/Dropbox/Apps/vim/html/'}]

"Plugin Tern_for_vim setting
setlocal omnifunc=tern#Complete
call tern#Enable()
runtime after/ftplugin/javascript_tern.vim
