" window management 
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-h> <C-W>h
nnoremap <C-l> <C-W>l
set splitbelow
set splitright

" save <M-s>
nnoremap <C-s> :w<cr>  "Works in normal mode, must press Esc first"
inoremap <C-s> <Esc>:w<cr>i "Works in insert mode, saves and puts back in insert mode"

" javascript run
